function rVec = calcBranchResidue(Tr,NdV, lev, rVec)
% summing the leaves of the tree starting from the vector Nd outwards
% This is done if a branch of the tree has no further matches, 
% hence we sum its leaves to the end
% NdV - possibly a vector of nodes


smT = 0;
smV = [];
lenV = length(NdV);
for i=1:lenV
    Wt = Tr(NdV(i)).Wt;
    if lev>1 % Don't add edges for first call
        smV(i) = sum(Tr(NdV(i)).Nd ~= 0); 
    end
    if sum(Wt) == 0
        continue
    end
    rVec = calcBranchResidue(Tr,Tr(NdV(i)).Nd(Wt~=0), lev+1, rVec);
end
if lev > length(rVec)
    rVec(lev)=sum(smV);
else
    rVec(lev) = rVec(lev) + sum(smV);
end



