% here we build an example of a binary tree using a sparse matrix
global globalTr1 globalTr2
maxDistLevel = 20
% The first tree

s1 = sparse(28,28);
for k=1:2:17
    s1(k,k+1)=1;
    s1(k,k+2)=1;
end
s1(9,20)=1;
for k=20:2:26
    s1(k,k+1)=1;
    s1(k,k+2)=1;
end

%Tree1PIC = biograph(A1);
%view(Tree1PIC)

%the second tree
s2 = sparse(32,32);
for k=1:2:15
    s2(k,k+1)=1;
    s2(k,k+2)=1;
end
s2(1,18)=1;
for k=18:2:30
    s2(k,k+1)=1;
    s2(k,k+2)=1;
end

% Set the center nodes
center1 = 1;
center2 = 1;
A1 = full(s1);
A1= A1+A1';
A2 = full(s2);
A2 = A2+A2';
Tree1 = generateTreeStruct(A1,center1);
Tree2 = generateTreeStruct(A2,center2);
globalTr1 = Tree1;
globalTr2 = Tree2;

% =================================================================
% NOTE: In general, if one is given a pair of trees, first run the
% 'findTreeCenter' function with input of adjacency matrix
% =================================================================

% now calculate the vectorial tree distance
Distance = pairTreeDist(globalTr1, globalTr2, center1,center2,1,zeros(1,maxDistLevel))

% the 'Distance' output is a vector

        
