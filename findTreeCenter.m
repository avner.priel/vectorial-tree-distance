function cent = findTreeCenter(adjM)
%
% This function get adjacency matrix of a TREE, and output its CENTER node
% by clipping the tree, starting from its outer leaves

N = size(adjM,1);

foundCent = 0;
remNodes = 1:N ;
iter = 1;
while foundCent==0
    % find leaves connected to boundary nodes
    candNodes = [];
    vsr = sum(adjM,2); % over rows
    candRows = find(vsr==1); % candidate nodes
    for i=1:length(candRows)
        ci = find(adjM(candRows(i),:)==1);
        con2ci = find(adjM(:,ci) >0);
        smgt1 = sum(sum(adjM(con2ci,:),2) > 1); % find all rows whos nodes are connected to more than 1
        if smgt1 == 1  %
            candNodes = [candNodes; candRows(i)];
        end
    end
    % Now trim candNodes from adjM
    remNodes(candNodes) = [];
    adjM(candNodes,:) = [];
    adjM(:,candNodes) = [];
    % Halt condition
    if any(sum(adjM,2)==(size(adjM,1)-1))
        break
    end
    iter = iter+1;
end

cent = remNodes(sum(adjM,2)==(size(adjM,1)-1));
cent = cent(1);

