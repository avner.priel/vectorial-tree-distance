function treeSt = generateTreeStruct(adjM, center)
%
% Starting from a given center-node and adjacency matrix, generate the 
% tree structure to be used by functions like 'permTreeDist'

% Tree structure
treeSt(1).Nd = [];
treeSt(1).Wt = [];

N = size(adjM,1);

remNodes = ones(1,N);
iter = 1;
curNodes = center;

while sum(remNodes) > 0
    % find leaves connected to boundary nodes
    if iter > 1
        curNodes = nextNodes;
    end
    nextNodes = [];
    for i=1:length(curNodes)
        candRows = find(adjM(curNodes(i),:)==1);
        treeSt(curNodes(i)).Nd = candRows;
        Wt=[];
        for j=1:length(candRows)
            Wt(j) = sum(adjM(candRows(j),:)==1)-1;
        end
        treeSt(curNodes(i)).Wt = Wt;
        nextNodes = [nextNodes candRows];
    end
    nextNodes = sort(nextNodes);
    remNodes(curNodes) = 0;
    adjM(curNodes,:) = 0;
    adjM(:,curNodes) = 0;
    iter = iter+1;
end


