function [permDvec] = pairTreeDist(globalTr1, globalTr2, nodeTr1, nodeTr2, levelR, permDvec)
% Main function to calculate the vectorial distance between a pair of trees
% *** This is a recursive function
%
% Tr1, Tr2 - global trees structures
% Tr1.W - weights of next level
% Tr1(Ni).N - nodes pointed by Ni
%
% Example (first time run): [permDvec] = pairTreeDist(globalTr1, globalTr2, 1, 1, 1, zeros(1,5))

%global globalTr1 globalTr2

Wt1 = globalTr1(nodeTr1).Wt;
Wt2 = globalTr2(nodeTr2).Wt;
Nd1 = globalTr1(nodeTr1).Nd;
Nd2 = globalTr2(nodeTr2).Nd;

% Just for the first level, compare number of edges
if levelR == 1
    permDvec(levelR) = abs(length(Nd1)-length(Nd2));
    levelR = 2;
end

sWt1 = sum(Wt1);
sWt2 = sum(Wt2);
if sWt1==0 || sWt2==0
    if sWt1==0 && sWt2 ~= 0
        rVec = sWt2; %[];
        vRes = calcBranchResidue(globalTr2, Nd2, 1, rVec);
        permDvec(levelR:(levelR+length(vRes)-1)) = vRes;
        return
    elseif sWt1~=0 && sWt2 == 0
        rVec = sWt1; %[];
        vRes = calcBranchResidue(globalTr1, Nd1, 1, rVec); 
        permDvec(levelR:(levelR+length(vRes)-1)) = vRes;
        return
    else
        return
    end
end

[idxPw, DPw] = permMatch(Wt1, Wt2); % calculate minimal distance weight matches

permDvec(levelR) = DPw; % vector of distances per level in this branch of the tree

sMin = [];
sMat = [];
for i = 1:size(idxPw,1)
    for j = 1:size(idxPw,2)
        if idxPw(i,j) <= length(Nd1) && j <= length(Nd2)
            [permDvec1] = pairTreeDist(globalTr1, globalTr2, Nd1(idxPw(i,j)), Nd2(j), levelR+1, permDvec);
        end
        if idxPw(i,j) > length(Nd1)
            permDvec1 = permDvec;
            vRes = calcBranchResidue(globalTr2, Nd2(j), 1, []);
            permDvec1(levelR:(levelR+length(vRes)-1)) = vRes;
        end
        if j > length(Nd2)
            permDvec1 = permDvec;
            vRes = calcBranchResidue(globalTr1, Nd1(idxPw(i,j)), 1, []);
            permDvec1(levelR:(levelR+length(vRes)-1)) = vRes;
        end
        if ~isempty(sMat) && length(permDvec1) > size(sMat,3)
            lent = min(size(sMat,3), length(permDvec));
            permDvec1 = permDvec1(1:lent);
        end
        if length(permDvec1) > length(permDvec)
            lent = length(permDvec);
            permDvec1 = permDvec1(1:lent);
        end
        sMat(i,j,:) = permDvec1;
    end
end

sm = squeeze(sum(sMat,2)); 
if size(sMat,1) == 1 % i.e., no permutation
    sMin = sm;
else
    relD = 1:size(sm,1);
    for i = levelR+1 : length(permDvec)
        tvec = sm(relD,i);
        mn = min(tvec);
        iD = find(tvec == mn);
        if length(iD)==1
            sMin = sm(relD(iD),:);
            break
        else
            relD = relD(iD);
            if i == length(permDvec)
                sMin = sm(relD(1),:);
            end
        end
    end
end

permDvec((levelR+1):end) = sMin((levelR+1):end);



