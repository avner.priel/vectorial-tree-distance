function [idxPw, DPw] = permMatch(Wt1, Wt2)
% calculate minimal distance weight matches
lenW1 = length(Wt1);
lenW2 = length(Wt2);
len = max(lenW1,lenW2);
if lenW1 < lenW2
    Wt1 = [Wt1 zeros(1,lenW2-lenW1)];
elseif lenW1 > lenW2
    Wt2 = [Wt2 zeros(1,lenW1-lenW2)];
end

% currently, the permMatch implementation is naive, 
% hence we limit the maximal size of the vectors. 
% There exists more efficient algorithms for that purpose 
% with polynomial order
if len>13
    error('permMatch: weight vector too BIG')
end

permIW = perms(1:len);
permWt1 = Wt1(permIW);
matWt2 = repmat(Wt2,size(permWt1,1),1);

difWvec = sum(abs(permWt1 - matWt2),2);

DPw = min(difWvec);
ii = find(difWvec == DPw);
idxPw = permIW(ii,:);

